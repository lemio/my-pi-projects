/*
 * test2.c:
 *   Simple test program to test the wiringPi functions
 *   PWM test
 */

#include <wiringPi.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
void sendTelegram(unsigned long data) {
   int pin = 7;
   unsigned int periodusec = (unsigned long)data >> 23;

   unsigned short repeats = 1 << (((unsigned long)data >> 20) & 7);

   data = data & 1048575; //truncate to 20 bit

      

   //Convert the base3-code to base4, to avoid lengthy calculations when transmitting.. Messes op timings.

   unsigned long dataBase4 = 0;

   
   unsigned short i;
   for (i=0; i<12; i++) {

      dataBase4<<=2;

      dataBase4|=(data%3);

      data/=3;

   }

   
   unsigned short int j;
   for (j=0;j<repeats;j++) {      

      //Sent one telegram      

      

      //Use data-var as working var

      data=dataBase4;
      unsigned short i;
      for (i=0; i<12; i++) {

         switch (data & 3) {

            case 0:
               digitalWrite(pin, HIGH);
               delayMicroseconds(periodusec);
               digitalWrite(pin, LOW);
               delayMicroseconds(periodusec*3);
               digitalWrite(pin, HIGH);
               delayMicroseconds(periodusec);
               digitalWrite(pin, LOW);
               delayMicroseconds(periodusec*3);
               break;

            case 1:

               digitalWrite(pin, HIGH);
               delayMicroseconds(periodusec*3);
               digitalWrite(pin, LOW);
               delayMicroseconds(periodusec);
               digitalWrite(pin, HIGH);
               delayMicroseconds(periodusec*3);
               digitalWrite(pin, LOW);
               delayMicroseconds(periodusec);
               break;

            case 2: //AKA: X or float

               digitalWrite(pin, HIGH);
               delayMicroseconds(periodusec);
               digitalWrite(pin, LOW);
               delayMicroseconds(periodusec*3);
               digitalWrite(pin, HIGH);
               delayMicroseconds(periodusec*3);
               digitalWrite(pin, LOW);
               delayMicroseconds(periodusec);

               break;

         }

         //Next trit

         data>>=2;

      }

      

      //Send termination/synchronisation-signal. Total length: 32 periods

      digitalWrite(pin, HIGH);
      delayMicroseconds(periodusec);
      digitalWrite(pin, LOW);
      delayMicroseconds(periodusec*31);

   }

}
int main (void)
{
   wiringPiSetup ();
   pinMode(7,OUTPUT);
   for(;;){
        sendTelegram(3148873752UL);
        delay(2000);
        sendTelegram(3148873754UL);
        delay(2000);   
   }
}
